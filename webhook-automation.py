#
#  _____ _____    ______    _          _      
# |_   _|  __ \  |  ____|  | |        (_)     
#   | | | |__) | | |__ __ _| |__  _ __ _  ___ 
#   | | |  ___/  |  __/ _` | '_ \| '__| |/ __|
#  _| |_| |      | | | (_| | |_) | |  | | (__ 
# |_____|_|      |_|  \__,_|_.__/|_|  |_|\___|
#
#                                              
#-------------------
# Name             Webhook-automation.py
# Description      Python script to demonstrate API calls from IP Fabric to PRTG, ServiceNow & Webex Teams.
#                  Created for the Paessler Alliance Partner Event, March 2022.
#-------------------
# Notes
#
# - 
# 
# - 
#
# - 
#-------------------
# License
#
# - Released under MIT License Copyright (c) 2022 Daren Fulwell
#
#-------------------
# Version History
# 
# Version  Date        Notes
# 1.0      14/03/2022  Initial Release
#
# ------------------
# Daren Fulwell - IP Fabric
# ------------------                                             

from flask import Flask, request, Response
import concurrent.futures
import logging
import queue
import random
import threading
import time
import json
import requests
from rich import print
from webexteamssdk import WebexTeamsAPI

app=Flask(__name__)

#Global variables
pipeline = queue.Queue(maxsize=10)

#Credentials & Target Servers - update as needed

ipfApiToken='xxxxxxxxxxxxxxxxxxxxxxxxxxxx' #Replace with API Token from IP Fabric console
ipfHeaders={'X-API-Token':ipfApiToken,'Content-Type':'application/json'}
ipfServer='xxxxxx.xxxxxx.xxxxxx' #Replace with FQDN of IP Fabric server
ipfBaseUrl='/api/v1/'
PRTGServer='xxx.xxx.xxx.xxx' #Replace with PRTG Server IP
PRTGUser='xxxxxxxx' #Replace with PRTG username
PRTGPass='xxxxxxxx' #Replpace with PRTG user password
sNowServer='xxxxxxxx.service-now.com' #Replace with FQDN of ServiceNow instance server
sNowUser='xxxxxxxx' #Replace with ServiceNow username
sNowPass='xxxxxxxx' #Replpace with ServiceNow user password
sNowHeaders={'Connection':'keep-alive','Content-Type':'application/json','Accept':'application/json'}


@app.route('/')
@app.route('/index')
def index():
    return 'Some random nonsense just to prove that the web server is working'
    
@app.route('/webhook',methods=['POST'])
def webhook():
    #Take message from the /webhook URL and place it on the queue
    pipeline.put(request.json)
    return Response(status=200)

def consumer(queue, event):
    #Pull message from queue and act on it

    while not event.is_set() or not queue.empty():
        #Lift the message off the queue
        message = queue.get()
        
        if message['type'] == 'snapshot':
            #If the message relates to snapshots, process it
            print('[blue]WEBHOOK > IPF SNAPSHOT '+message['status'].upper()+'[/blue]')
            
            if (message['status']=='completed') or (message['status']=='loaded'):
                #Fetch last and previous snapshots info from IP Fabric
                snapEndpoint='https://'+ipfServer+ipfBaseUrl+'snapshots'
                snapRequest=requests.get(snapEndpoint,headers=ipfHeaders,verify=False)
                snaps=json.loads(snapRequest.text)
                lastLoaded=False
                prevLoaded=False
                for snap in snaps:
                    if snap['state']=='loaded':
                        if not lastLoaded:
                            lastSnap=(snap['id'],snap['name'])
                            lastLoaded=True
                            print("  [green]IPF - SNAPSHOT - Found lastLoaded ("+lastSnap[0]+","+lastSnap[1]+")[/green]")
                        elif not prevLoaded:
                            prevSnap=(snap['id'],snap['name'])
                            prevLoaded=True
                            print("  [green]IPF - SNAPSHOT - Found prevLoaded ("+prevSnap[0]+","+prevSnap[1]+")[/green]")

                if lastLoaded and prevLoaded:
                    #Fetch inventory differences between last and previous IP Fabric snapshots
                    diffEndpoint='https://'+ipfServer+ipfBaseUrl+'tables/management/changes/devices'
                    diffFilter='{"columns": ["hostname","loginIp","sn"],"filters": {"status":["eq","added"]},"bindVariables": {"after": "'+lastSnap[0]+'","before": "'+prevSnap[0]+'"}}'
                    devs={"data":[],"_meta":{"limit":None,"start":0,"count":1,"size":0}}

                    #check that IPF returns the actual differences
                    while len(devs['data'])!=devs['_meta']['count']:
                        #if not, wait one second and try again!
                        time.sleep(1)
                        diffRequest=requests.post(diffEndpoint,diffFilter,headers=ipfHeaders,verify=False)
                        devs=json.loads(diffRequest.text)

                    print("  [green]IPF - SNAPSHOT - Fetched "+str(len(devs['data']))+" differences between lastLoaded and prevLoaded[/green]")

                    #Check for added devices to see if SNMP configured or otherwise and create Snow inventory as required
                    unconfiguredDevices=[]
                    badlyConfiguredDevices=[]
                    configuredDevices=[]
                    vendDict={}
                    locDict={}

                    #Loop through all added devices 
                    for dev in devs['data']:
                        
                        #API call to IP Fabric to count SNMP communities for device
                        snmpEndpoint='https://'+ipfServer+ipfBaseUrl+'tables/management/snmp/summary'
                        snmpFilter='{"columns": ["hostname","sn","communitiesCount"],"filters":{"sn":["eq","'+dev["sn"]+'"]},"snapshot":"'+lastSnap[0]+'"}'
                        snmpRequest=requests.post(snmpEndpoint,snmpFilter,headers=ipfHeaders,verify=False)
                        snmpConfigured=(json.loads(snmpRequest.text)['data'][0]['communitiesCount']>0)

                        if not snmpConfigured:
                            #add device to list of unconfigured devices
                            unconfiguredDevices.append(dev)
                            print("    [red]IPF  - SNMP not configured on "+dev["hostname"]+"[/red]")
                        else:
                            #API request to collect all configured SNMP communities for that device from IP Fabric
                            commEndpoint='https://'+ipfServer+ipfBaseUrl+'tables/management/snmp/communities'
                            commFilter='{"columns": ["hostname","sn","name","authorization","acl"],"filters":{"sn":["eq","'+dev["sn"]+'"]},"snapshot":"'+lastSnap[0]+'"}'
                            commRequest=requests.post(commEndpoint,commFilter,headers=ipfHeaders,verify=False)
                            comms=json.loads(commRequest.text)

                            #Select "the best" SNMP community from the list
                            chosenComm={"community":"","auth":"","acl":True}
                            #Loop through all communities
                            for comm in comms['data']:
                                #And select the best match
                                if chosenComm['community']=="":
                                    replaceComm=comm['name'] != ""
                                elif chosenComm['auth']=="read-write" and chosenComm['acl']:
                                    replaceComm=comm['authorization']=="read-only" or not comm['acl']
                                elif chosenComm['auth']=="read-write" and not chosenComm['acl']:
                                    replaceComm=comm['authorization']=="read-only"
                                elif chosenComm['auth']=="read-only" and chosenComm['acl']:
                                    replaceComm=not comm['acl']
                                else:
                                    replaceComm=False

                                if replaceComm:
                                    chosenComm['community']=comm['name']
                                    chosenComm['auth']=comm['authorization']
                                    chosenComm['acl']=(len(comm['acl'])>0)

                            #kludge for Paessler demo - only accept "ipfabric" as community string
                            if chosenComm['community']=='ipfabric':
                                #add device to the list of configured devices
                                configuredDevices.append(dev)
                                print("    [green]IPF  - "+dev['hostname']+" is using SNMP v2 community "+chosenComm['community']+"[/green]")
                            else:
                                badlyConfiguredDevices.append(dev)
                                print("    [gold3]IPF  - "+dev['hostname']+" is using SNMP v2 community "+chosenComm['community']+" - needs reconfiguration[/gold3]")
                                snmpConfigured=False

                        #API call to create device in PRTG and pause it
                        duplicateEndpoint='http://'+PRTGServer +'/api/duplicateobject.htm'
                        duplicateParams='name='+dev['hostname']+'&host='+dev['loginIp']+'&username='+PRTGUser+'&password='+PRTGPass+'&id=12849&targetid=12869&show=nohtmlencode'
                        createRequest=requests.post(duplicateEndpoint+'?'+duplicateParams, headers={}, json={}, verify=False)

                        if createRequest.ok:
                            createdId=(createRequest.url.split('id%3D')[1]).split('&')[0]

                            #API call to tag device in PRTG with IP Fabric snapshot info
                            tagEndpoint='http://'+PRTGServer+'/api/setobjectproperty.htm'
                            tagParams='id='+str(createdId)+'&username='+PRTGUser+'&password='+PRTGPass+'&name=tags&value=IP_Fabric_'+lastSnap[1].replace(" ","_")
                            tagRequest=requests.post(tagEndpoint+'?'+tagParams, headers={}, json={}, verify=False)

                            if snmpConfigured:
                                #API call to update PRTG SNMP community for device
                                snmpEndpoint='http://'+PRTGServer+'/api/setobjectproperty.htm'
                                snmpParams='id='+str(createdId)+'&username='+PRTGUser+'&password='+PRTGPass+'&name=snmpcommv2&value='+chosenComm['community']
                                snmpRequest=requests.post(snmpEndpoint+'?'+snmpParams, headers={}, json={}, verify=False)
                        
                                if snmpRequest.ok:
                                    #API call to resume device in PRTG
                                    resumeEndpoint='http://'+PRTGServer+'/api/pause.htm'
                                    resumeParams='id='+str(createdId)+'&username='+PRTGUser+'&password='+PRTGPass+'&action=1'
                                    resumeRequest=requests.post(resumeEndpoint+'?'+resumeParams, headers={}, json={}, verify=False)
                                    if resumeRequest.ok:
                                        print("    [green]PRTG - Update complete for "+dev['hostname']+" and discovery triggered[/green]")
                            else:
                                #Device not configured for SNMP so pause it in PRTG
                                print("    [gold3]PRTG - Update complete for "+dev['hostname']+" and device paused awaiting SNMP config[/gold3]")
                        
                        #Fetch vendor and location info for device
                        devIPFEndpoint='https://'+ipfServer+ipfBaseUrl+'tables/inventory/devices'
                        devIPFFilter='{"columns": ["hostname","vendor","siteName"],"filters": {"hostname":["eq","'+dev['hostname']+'"]},"snapshot":"'+lastSnap[0]+'"}'
                        devIPFRequest=requests.post(devIPFEndpoint,devIPFFilter,headers=ipfHeaders,verify=False)
                        devDeets=json.loads(devIPFRequest.text)
                        
                        #Fetch SNow vendor and site lists if we don't already have them
                        if (vendDict=={}):
                            vendEndpoint='https://'+ sNowServer + '/api/now/v1/table/core_company'
                            vendRequest=requests.get(vendEndpoint,auth=(sNowUser,sNowPass),headers=sNowHeaders)
                            vendDict=json.loads(vendRequest.text)

                        if (locDict=={}):
                            locEndpoint='https://'+ sNowServer + '/api/now/v1/table/cmn_location'
                            locRequest=requests.get(locEndpoint,auth=(sNowUser,sNowPass),headers=sNowHeaders)
                            locDict=json.loads(locRequest.text)
  
                        #loop through vendor list till we find a match
                        testVendor=devDeets['data'][0]['vendor']
                        for vendor in vendDict['result']:
                            if vendor['name'] == testVendor.capitalize():
                                vendId=vendor['sys_id']
                                break

                        #loop through location list till we find a match
                        testSite=devDeets['data'][0]['siteName']
                        for loc in locDict['result']:
                            #breakpoint()
                            if loc['name'].lower() == testSite.lower(): #capitalize():
                                locId=loc['sys_id']
                                break

                        #Create new ServiceNow inventory item
                        deviceData={"attributes": {"sys_class_name": "cmdb_ci_netgear","discovery_source":"ServiceNow","manufacturer": vendId,"serial_number": dev['sn'], "location": locId,"name": dev['hostname'],"ip_address": dev['loginIp']}}
                        deviceEndpoint='https://'+ sNowServer + '/api/now/v1/cmdb/instance/cmdb_ci_netgear'
                        deviceRequest=requests.post(deviceEndpoint,auth=(sNowUser,sNowPass),headers=sNowHeaders,data=json.dumps(deviceData))
                        print("    [green]SNow - Created CMDB item for "+dev['hostname']+"[/green]")
                    	
                    #Prepare for Webex Teams update 
                    webexTeamsRoom=WebexTeamsAPI()
                    myWebexRoom='Y2lzY29zcGFyazovL3VzL1JPT00vNmUwYTQ2MzAtNTAzNC0xMWVhLTk2OTgtODMwZmVjZGNhOGM4'
                    
                    #Create an informational ServiceNow incident for devices configured for SNMP and added to PRTG 
                    if len(configuredDevices)>0:
                        #Build the notes for the incident
                        notes="IP Fabric triggered automatic update added the following devices to PRTG, auto-discovered and monitoring:\n\n"
                        for dev in configuredDevices:
                            notes+=dev['hostname']+' ('+dev['loginIp']+')\n'

                         
                        #API request to ServiceNow to create incident
                        incidentData={"short_description":"INFO: Newly added to PRTG","sys_updated_by": "ip.fabric","urgency":"3","impact":"3","description":"Incident created by IP Fabric snapshot event. See comments below for list of devices automatically added to inventory","work_notes": notes,"caller_id":{"value":"2b95db0ddbf71010266e64d748961998"}}
                        incidentEndpoint='https://'+ sNowServer + '/api/now/table/incident'
                        incidentRequest=requests.post(incidentEndpoint,auth=(sNowUser,sNowPass),headers=sNowHeaders,data=json.dumps(incidentData))
                        if incidentRequest.ok:
                            print("    [green]SNow - Raised informational incident for new monitored inventory[/green]")
                        
                        #Update WebexTeams room
                        incidentInfo=json.loads(incidentRequest.text)['result']
                        incidentNum=incidentInfo['number']
                        incidentLink= 'https://'+sNowServer+'/incident.do?sys_id='+incidentInfo['sys_id']
                        response=webexTeamsRoom.messages.create(roomId=myWebexRoom,text=time.ctime()+' - '+incidentNum+' raised\n'+incidentLink+'\n\n'+notes+'\n\n')
                        if response.id != None:
                            print("    [green]WxT  - Submitted message to Webex Teams room[/green]")

                    #Create an action ServiceNow incident for devices wrongly configured with incorrect SNMP details added and paused in PRTG 
                    if len(badlyConfiguredDevices)>0:
                        #Build the notes for the incident
                        notes="IP Fabric triggered automatic update added the following devices to PRTG, paused and awaiting device reconfiguration:\n\n"
                        for dev in badlyConfiguredDevices:
                            notes+=dev['hostname']+' ('+dev['loginIp']+')\n'
                        
                        #API request to ServiceNow to create incident
                        incidentData={"short_description":"ACTION: Newly discovered - require SNMP config change","sys_updated_by": "ip.fabric","urgency":"1","impact":"1","description":"Incident created by IP Fabric snapshot event. See comments below for list of devices discovered by IP Fabric.  Device configuration requires a change to meet standards to enable SNMP polling, then resume in PRTG.","work_notes": notes,"caller_id":{"value":"2b95db0ddbf71010266e64d748961998"}}
                        incidentEndpoint='https://'+ sNowServer + '/api/now/table/incident'
                        incidentRequest=requests.post(incidentEndpoint,auth=(sNowUser,sNowPass),headers=sNowHeaders,data=json.dumps(incidentData))
                        if incidentRequest.ok:
                            print("    [green]SNow - Raised incident to make SNMP config changes for unmonitored devices[/green]")
                        
                        #Update WebexTeams room
                        incidentInfo=json.loads(incidentRequest.text)['result']
                        incidentNum=incidentInfo['number']
                        incidentLink='https://'+sNowServer+'/incident.do?sys_id=' +incidentInfo['sys_id']
                        response=webexTeamsRoom.messages.create(roomId=myWebexRoom,text=time.ctime()+' - '+incidentNum+' raised\n'+incidentLink+'\n\n'+notes+'\n\n')
                        if response.id != None:
                            print("    [green]WxT  - Submitted message to Webex Teams room[/green]")


                    #Create an action ServiceNow incident for devices not yet configured for SNMP and added and paused in PRTG 
                    if len(unconfiguredDevices)>0:
                        #Build the notes for the incident
                        notes="IP Fabric triggered automatic update added the following devices to PRTG, paused and awaiting device configuration:\n\n"
                        for dev in unconfiguredDevices:
                            notes+=dev['hostname']+' ('+dev['loginIp']+')\n'
                        
                        #API request to ServiceNow to create incident
                        incidentData={"short_description":"ACTION: Newly discovered - require new SNMP config","sys_updated_by": "ip.fabric","urgency":"1","impact":"1","description":"Incident created by IP Fabric snapshot event. See comments below for list of devices discovered by IP Fabric.  Device configuration required to enable SNMP polling, then resume in PRTG.","work_notes": notes,"caller_id":{"value":"2b95db0ddbf71010266e64d748961998"}}
                        incidentEndpoint='https://'+ sNowServer + '/api/now/table/incident'
                        incidentRequest=requests.post(incidentEndpoint,auth=(sNowUser,sNowPass),headers=sNowHeaders,data=json.dumps(incidentData))
                        if incidentRequest.ok:
                            print("    [green]SNow - Raised incident to configure SNMP for unmonitored devices[/green]")
                        
                        #Update WebexTeams room
                        incidentInfo=json.loads(incidentRequest.text)['result']
                        incidentNum=incidentInfo['number']
                        incidentLink='https://'+sNowServer+'/incident.do?sys_id=' +incidentInfo['sys_id']
                        response=webexTeamsRoom.messages.create(roomId=myWebexRoom,text=time.ctime()+' - '+incidentNum+' raised\n'+incidentLink+'\n\n'+notes+'\n\n')
                        if response.id != None:
                            print("    [green]WxT  - Submitted message to Webex Teams room[/green]")
                    
                else:
                    print("  [red]IPF - SNAPSHOT - lastLoaded and prevLoaded not found[/red]")

                print("[blue]IPF SNAPSHOT COMPLETE actions completed.[/blue]")
                print("")
        else:
            print('[magenta]WEBHOOK > IPF INTENT[/magenta]')

    print("---- Consumer received event. Exiting ----")


if __name__ == "__main__":
    requests.packages.urllib3.disable_warnings(requests.packages.urllib3.exceptions.InsecureRequestWarning)
    event = threading.Event()
    with concurrent.futures.ThreadPoolExecutor(max_workers=1) as executor:
        executor.submit(consumer, pipeline, event)
        app.run(debug=True,host='0.0.0.0')
        time.sleep(30)
        logging.info("Main: about to set event")
        event.set()
