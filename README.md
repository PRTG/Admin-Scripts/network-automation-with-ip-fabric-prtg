# Network Automation with IP Fabric + PRTG

Sample network automation Python script that demonstrates how APIs can be used to pass data between applications which can then act upon the received information.

## More Information

The script is provided as an example of an automation project only. It was built for a specific demo environment and will absolutely NOT work in your environment, without major changes. However, it does show how APIs can be used to pass data between IP fabric, PRTG, ServiceNow and Webex Teams. It’s intended to be used as an example from which you can develop your own automation tooling.

A description of the script and what it does can be found in this blog article:
https://blog.paessler.com/network-automation-do-more-with-less

The script was written by Daren Fulwell of IP Fabric and is released for use under the MIT license. The script is provided as an example only and is unsupported.
